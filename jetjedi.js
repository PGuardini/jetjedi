angular.module('jedi',[])
    .controller('ctrl', function($scope){
        $scope.jedis = [
            {name:'Anakin Skywalker',planet:'Tatooine',status:'Padawan',master:'Obi Wan Kenobi'},
            {name:'Obi Wan Kenobi',planet:'Stewjon',status:'Jedi Master',master:'Quin Gon Jinn'},
            {name:'Quin Gon Jinn',planet:'Unknown',status:'Jedi Master',master:'Dookan'}
        ];
});